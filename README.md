# MRIBuilder

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://open.win.ox.ac.uk/pages/ndcn0236/mribuilder.jl/dev/)
[![Build Status](https://git.fmrib.ox.ac.uk/ndcn0236/mribuilder.jl/badges/main/pipeline.svg)](https://git.fmrib.ox.ac.uk/ndcn0236/mribuilder.jl/pipelines)
[![Coverage](https://git.fmrib.ox.ac.uk/ndcn0236/mribuilder.jl/badges/main/coverage.svg)](https://git.fmrib.ox.ac.uk/ndcn0236/mribuilder.jl/commits/main)

The latest documentation can be found [here](https://open.win.ox.ac.uk/pages/ndcn0236/mribuilder.jl/dev).