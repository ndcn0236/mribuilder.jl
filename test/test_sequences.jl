@testset "test_sequences.jl" begin
    @testset "GradientEcho" begin
        seq = GradientEcho(TE=40)
        @test length(seq) == 4
        @test variables.duration(seq) ≈ 40
        @test all(isapprox.(variables.duration.(seq), [0., 40., 0., 0.], atol=1e-6))
        @test length(collect(iter_instant_pulses(seq))) == 1
        @test length(collect(iter_instant_gradients(seq))) == 0.
        @test variables.duration_dephase(seq) ≈ 40.
        @test variables.duration_transverse(seq) ≈ 40.
    end
    @testset "SpinEcho" begin
        seq = SpinEcho(TE=40)
        @test length(seq) == 6
        @test variables.duration(seq) ≈ 40
        @test all(isapprox.(variables.duration.(seq), [0., 20., 0., 20., 0., 0.], atol=1e-6))
        @test length(collect(iter_instant_pulses(seq))) == 2
        @test length(collect(iter_instant_gradients(seq))) == 0.
        @test variables.duration_dephase(seq) ≈ 0. atol=1e-4
        @test variables.duration_transverse(seq) ≈ 40.
    end
    
    @testset "DW-SE" begin
        min_rise_time = Default_Scanner.gradient / Default_Scanner.slew_rate
        @testset "Instant pulse & readout" begin
            @testset "Default behaviour is to minimise duration" begin
                seq = DiffusionSpinEcho(bval=1.)
                @test length(seq) == 10
                grad_duration = variables.TE(seq) / 2
                @test all(isapprox.(variables.duration.(seq), [0., 0., grad_duration, 0., 0., 0., grad_duration, 0., 0., 0.], atol=1e-6))
                @test length([iter_instant_pulses(seq)...]) == 2
                @test length([iter_instant_gradients(seq)...]) == 0.
                @test variables.bval(seq) ≈ 1.
                @test 40. < variables.TE(seq) < 50.
                @test variables.duration(seq) ≈ variables.TE(seq)
                @test variables.rise_time(seq[:gradient]) ≈ min_rise_time rtol=1e-4
                @testset "Explicitly minimising TE gives same result" begin
                    alt = DiffusionSpinEcho(bval=1., TE=:min)
                    @test all(isapprox.(variables.duration.(seq), variables.duration.(alt), atol=1e-4, rtol=1e-6))
                end
            end
            @testset "Maximise b-value" begin
                seq = DiffusionSpinEcho(TE=80., bval=:max)
                @test length(seq) == 10
                @test all(isapprox.(variables.duration.(seq), [0., 0., 40., 0., 0., 0., 40., 0., 0., 0.], atol=1e-4, rtol=1e-4))
                @test length([iter_instant_pulses(seq)...]) == 2
                @test length([iter_instant_gradients(seq)...]) == 0.
                @test variables.TE(seq) ≈ 80. rtol=1e-6
                @test variables.duration(seq) ≈ 80. rtol=1e-6
                @test 4.8 < variables.bval(seq) < 4.9
                @test variables.rise_time(seq[:gradient]) ≈ min_rise_time rtol=1e-4
                @test all(isapprox.(edge_times(seq, tol=1e-3), [0., min_rise_time, 40. - min_rise_time, 40, 40 + min_rise_time, 80 - min_rise_time, 80.], atol=1e-4))

                # can also maximise q-value
                seq2 = DiffusionSpinEcho(TE=80., qval=:max)
                @test all(isapprox.(variables.duration.(seq), variables.duration.(seq2), atol=1e-4, rtol=1e-4))
                @test variables.TE(seq) ≈ variables.TE(seq2) atol=1e-4 rtol=1e-4
                @test variables.bval(seq) ≈ variables.bval(seq2) atol=1e-4 rtol=1e-4
            end
            @testset "Set diffusion time Δ" begin
                seq = DiffusionSpinEcho(TE=80., Δ=70., qval=:max)
                @test all(isapprox.(variables.duration.(seq), [0., 0., 10., 30., 0., 30., 10., 0., 0., 0.], atol=1e-4, rtol=1e-4))
                @test variables.Δ(seq) ≈ 70.
                @test variables.TE(seq) ≈ 80.
                @test variables.duration(seq) ≈ 80.
                @test 0.72 < variables.bval(seq) < 0.73
                @test variables.readout_times(seq)[1] ≈ variables.TE(seq)
                @test variables.rise_time(seq[:gradient]) ≈ min_rise_time rtol=1e-4
                @test all(isapprox.(edge_times(seq), [0., min_rise_time, 10. - min_rise_time, 10., 40, 70, 70 + min_rise_time, 80 - min_rise_time, 80.], atol=1e-4))
            end
            @testset "Set gradient duration" begin
                seq = DiffusionSpinEcho(TE=80., gradient=(duration=10., ), bval=:max)
                @test all(isapprox.(variables.duration.(seq), [0., 0., 10., 30., 0., 30., 10., 0., 0., 0.], atol=1e-4, rtol=1e-4))
                @test variables.Δ(seq) ≈ 70. rtol=1e-4
                @test variables.TE(seq) ≈ 80.
                @test variables.duration(seq) ≈ 80. rtol=1e-4
                @test 0.72 < variables.bval(seq) < 0.73
                @test variables.readout_times(seq)[1] ≈ variables.TE(seq)
                @test variables.rise_time(seq[:gradient]) ≈ min_rise_time rtol=1e-4
            end
        end
        @testset "DW-SE with finite RF pulses" begin
            @testset "slice-select DW-SE" begin
                seq = DiffusionSpinEcho(duration=:min, bval=2., slice_thickness=2.)
                @test length(seq) == 10
                @test variables.duration(seq[1]) > 1.
                for index in 1:9
                    if index in [2, 4, 8, 9]
                        @test abs(variables.duration(seq[index])) < 1e-6
                    else
                        @test abs(variables.duration(seq[index])) > 1
                    end
                end
                @test variables.duration(seq[:gradient]) ≈ variables.duration(seq[:gradient2])
                @test variables.bval(seq) ≈ 2.
                @test length(variables.readout_times(seq)) == 1
                @test variables.readout_times(seq)[1] > variables.TE(seq)
            end
            @testset "voxel-wise DW-SE" begin
                seq = DiffusionSpinEcho(duration=:min, bval=2., voxel_size=2., fov=(20, 20, 20))
                @test length(seq) == 10
                @test variables.duration(seq[1]) > 1.
                for index in 1:9
                    if index in [2, 6, 8]
                        @test abs(variables.duration(seq[index])) < 1e-4
                    else
                        @test abs(variables.duration(seq[index])) > 1
                    end
                end
                @test variables.duration(seq[:gradient]) ≈ variables.duration(seq[:gradient2])
                @test variables.bval(seq) ≈ 2. rtol=1e-4
                @test length(variables.readout_times(seq)) > 50
                @test 67 < variables.TE(seq) < 68
                @test 72 < variables.duration(seq) < 73

                (pulse, t_pulse) = get_pulse(seq, 1.)
                @test 1. - t_pulse ≈ min_rise_time rtol=1e-4
                @test variables.flip_angle(pulse) ≈ 90. rtol=1e-6
                @test iszero(variables.phase(pulse))
                @test iszero(variables.phase(seq, 1.))
                @test iszero(variables.frequency(seq, 1.))

                @test isnothing(get_pulse(seq, 10.))
                @test isnan(variables.phase(seq, 10.))
                @test isnan(variables.frequency(seq, 10.))
    
                gp = GenericPulse(pulse, 0., 1.)
                @test gp.amplitude[1] ≈ 0. atol=1e-8
                @test gp.amplitude[end] ≈ variables.amplitude(pulse, 1.) rtol=1e-2
                @test all(iszero.(gp.phase))

                (pulse, t_pulse) = get_pulse(seq, 35.)
                @test 1.1 < t_pulse < 1.2
                @test variables.flip_angle(pulse) ≈ 180.
                @test iszero(variables.phase(pulse))
            end
        end
    end
end