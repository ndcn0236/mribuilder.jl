These example sequence files have been taken from the pulseq/tutorials github repository (https://github.com/pulseq/tutorials/tree/main/01_from_FID_to_PRESS)

These are all v1.4.0 and are used to test the reading of such pulseq files.