module ADCs
import JuMP: @constraint, value
import ...AbstractTypes: ReadoutComponent
import ....BuildSequences: fixed
import ....Variables: VariableType, apply_simple_constraint!, set_simple_constraints!, get_free_variable, make_generic, variables, @defvar


"""
    ADC(; center_halfway=true, oversample=1, variables...)

Adds a readout event.

## Parameters
- `center_halfway`: by default the `time_to_center` is assumed to be half of the `duration`. Set this to false to disable this assumption.
- `oversample`: by how much the ADC should oversample (minimum of 1).

## Variables
- `resolution`: number of voxels in the readout direction. This can be a non-integer value during optimisation.
- `nsamples`: number of samples in the readout. This can be a non-integer value during optimisation.
- `dwell_time`: Time between each readout sample in ms.
- `duration`: Total duration of the ADC event in ms.
- `time_to_center`: time till the center of k-space from start of ADC in ms.
- `effective_time`: same as `time_to_center`.
"""
struct ADC <: ReadoutComponent
    resolution :: VariableType
    dwell_time :: VariableType
    time_to_center :: VariableType
    oversample :: VariableType
end

function ADC(; resolution=nothing, dwell_time=nothing, time_to_center=nothing, center_halfway=true, oversample=nothing, kwargs...)
    res = ADC(
        get_free_variable(resolution; integer=true),
        get_free_variable(dwell_time),
        get_free_variable(time_to_center),
        get_free_variable(oversample, integer=true),
    )
    apply_simple_constraint!(res.dwell_time, :>=, 0)
    apply_simple_constraint!(res.oversample, :>=, 1)
    apply_simple_constraint!(res.resolution, :>=, 1)
    if center_halfway
        apply_simple_constraint!(variables.duration(res), 2 * res.time_to_center)
    else
        apply_simple_constraint!(res.time_to_center, :>=, 0)
        apply_simple_constraint!(res.time_to_center, :<=, variables.duration(res))
    end
    set_simple_constraints!(res, kwargs)
    return res
end

@defvar readout begin
    oversample(adc::ADC) = adc.oversample
    dwell_time(adc::ADC) = adc.dwell_time
    time_to_center(adc::ADC) = adc.time_to_center
    resolution(adc::ADC) = adc.resolution
end

"""
    oversample(adc)

The oversampling rate of the ADC readout.
"""
variables.oversample

"""
    dwell_time(adc)

The dwell time of the ADC readout in ms.
"""
variables.dwell_time

"""
    time_to_center(adc)

The time of the ADC readout to reach the center of k-space.
"""
variables.time_to_center

"""
    resolution(readout)

Resolution of the readout.
"""
variables.resolution

@defvar readout nsamples(adc::ADC) = variables.resolution(adc) * variables.oversample(adc)

"""
    nsamples(adc)

Number of samples in an ADC.
"""
variables.nsamples

@defvar readout_times(adc::ADC) = ((1:Int(variables.nsamples(adc))) .- 0.5) .* variables.dwell_time(adc)
@defvar begin
    duration(adc::ADC) = variables.nsamples(adc) * variables.dwell_time(adc)
    effective_time(adc::ADC) = variables.time_to_center(adc)
end

make_generic(adc::ADC) = adc

end